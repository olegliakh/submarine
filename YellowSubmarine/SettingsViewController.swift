//
//  SettingsViewController.swift
//  YellowSubmarine
//
//  Created by Oleg Liakh on 16.12.21.
//

import UIKit

let fontName = "Bicubik"
// MARK: - class
class SettingsViewController: UIViewController {
    
    // MARK: - lets/vars
    let imageSea = UIImage(named: "")
    var playerName = ""
    var settings = Settings(name: "", speed: 1, submarineImageName: "red")
    var records = Record(playerName: "", date: Date(), result: 2)
    // MARK: - IBOUtlets
    
    
    @IBOutlet weak var recordButton: UIButton!
    
    @IBOutlet weak var settingsButton: UIButton!
    
    @IBOutlet weak var redAlertLabel: UILabel!
    
    
    @IBOutlet weak var seaSettingsImageView: UIImageView!
    
    @IBOutlet weak var redMirorImageView: UIImageView!
    
    
    @IBOutlet weak var newGameButton: UIButton!
    
    @IBOutlet weak var quitButton: UIButton!
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        let button = UIButton(type: .roundedRect)
        button.frame = CGRect(x: 20, y: 50, width: 100, height: 30)
        button.setTitle("Test Crash", for: [])
        button.addTarget(self, action: #selector(self.crashButtonTapped(_:)), for: .touchUpInside)
        view.addSubview(button)
        
    }
    @IBAction func crashButtonTapped(_ sender: AnyObject) {
         let numbers = [0]
         let _ = numbers[1]
     }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        self.seaSettingsImageView.frame.origin = CGPoint(x: -40, y: -20)
        self.seaSettingsImageView.image = UIImage(named: "bottomThird")
        self.redMirorImageView.image = UIImage(named: "redMiror")
        self.seaSettingsImageView.addSubview(redMirorImageView)
        self.newGameButton.funGradient()
        self.settingsButton.roundCorners()
        self.redAlertLabel.textDropShadow(opacity: 6)
        self.redAlertLabel.dropShadow(colors: UIColor.white.cgColor, opacity: 1)
        self.recordButton.roundCorners()
        self.newGameButton.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        self.newGameButton.backgroundColor = .red
        self.newGameButton.layer.cornerRadius = 15
        attribText()
        self.newGameButton.setTitleWithAttributes(name: "Start".localized, font: fontName, size: 24)
        moveThirdSea(seaView: self.seaSettingsImageView)
        moveRedMiror()
        loadSettings()
        let recordTitle = "records".localized
        self.recordButton.setTitle(recordTitle, for: .normal)
        let settingsTitle = "Settings".localized
        self.settingsButton.setTitle(settingsTitle, for: .normal)
        let gameTitle = "Start".localized
        self.newGameButton.setTitle(gameTitle, for: .normal)

        
        
    }
    // MARK: - IBActions
    
    
    @IBAction func recordButtonPressed(_ sender: UIButton) {
        
//        guard  let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RxRecordViewController") as? RxRecordViewController else {return}
//        self.navigationController?.pushViewController(controller, animated: true)
        guard  let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "RecordViewController") as? RecordViewController else {return}
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func newGameButtonPressed(_ sender: UIButton) {
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "GameViewController") as?
                GameViewController else { return }
        controller.settings = settings
        self.navigationController?.pushViewController(controller, animated: true)
        
    }
    
    @IBAction func settingsButtonPressed(_ sender: UIButton) {
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SetSettingsViewController") as?
                SetSettingsViewController else {return}
        controller.settings = settings
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    
    // MARK: - flow func
    
    func loadSettings(){
        guard let settings = UserDefaults.standard.value(Settings.self, forKey: "SetSet") else {return}
        self.settings = settings
    }
    
    func  moveRedMiror () {
        UIView.animate(withDuration: 9, delay: 0, options: .curveLinear) {
            self.redMirorImageView.frame.origin.x -= 15
            
            
            self.redMirorImageView.frame.origin.y -= 5
            self.redMirorImageView.frame.origin.y += 5
        } completion: { _ in
            UIView.animate(withDuration: 9, delay: 0, options: .curveLinear) {
                self.redMirorImageView.frame.origin.x += 15
                
            } completion: { _ in
                self.moveRedMiror()
            }
            
        }
        func changeColorButton () {
            self.newGameButton.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
            self.newGameButton.backgroundColor = .red
        }}
    
    func attribText () {
        let myString = "Start"
        let attributes : [NSAttributedString.Key : Any] = [
            .font: UIFont(name: "Bicubik", size: 24)
        ]
        let attributedString = NSAttributedString(string: myString, attributes: attributes)
        self.newGameButton.setAttributedTitle(attributedString, for: .normal)
    }
}

func  moveThirdSea (seaView: UIImageView) {
    UIView.animate(withDuration: 9, delay: 0, options: .curveLinear) {
        seaView.frame.origin.x -= 13
        seaView.frame.origin.y -= 5
        seaView.frame.origin.y += 5
    } completion: { _ in
        UIView.animate(withDuration: 9, delay: 0, options: .curveLinear) {
            seaView.frame.origin.x += 13
        } completion: { _ in
            moveThirdSea(seaView: seaView)
            
        }
    }
}
