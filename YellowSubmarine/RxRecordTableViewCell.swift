
import UIKit

class RxRecordTableViewCell: UITableViewCell {

    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    
    static let identifier =  "RxRecordTableViewCell"
    
    func configure (name: String, result: Int, date: Date) {
        self.nameLabel.text = name
        self.resultLabel.text = String(result)
        let formater = DateFormatter ()
        formater.dateFormat = "dd.MM.yy HH:mm"
        formater.locale = Locale(identifier: "en_US")
        self.dateLabel.text = formater.string(from: date)
    }
    

}
