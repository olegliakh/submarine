import RxCocoa
import RxSwift
import UIKit

class RxRecordViewController: UIViewController {
    @IBOutlet weak var recordTableView: UITableView!
    
    let viewModel = RxTableModel()
    let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.setRecords(array: loadRecords())
        print(loadRecords().count)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        config()
    }
    
    private func config () {
        viewModel
            .data
            .bind(to: recordTableView.rx.items(cellIdentifier: RxRecordTableViewCell.identifier, cellType: RxRecordTableViewCell.self)) { index, record , cell  in
                cell.configure(name: record.playerName, result: record.result  , date:record.date )
            }
            .disposed(by: disposeBag)
    }
    
    func loadRecords() -> [Record] {
        guard let records = UserDefaults.standard.value([Record].self, forKey: "records")
        else {return []}
        return records
    }

}
