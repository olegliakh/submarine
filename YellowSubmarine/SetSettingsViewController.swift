//
//  SetSettingsViewController.swift
//  YellowSubmarine
//
//  Created by Oleg Liakh on 13.01.22.
//

import UIKit
// MARK: - class
class SetSettingsViewController: UIViewController {
    // MARK: - lets/vars
    var currentIndex = 0
    let submarineArray : [String] = [""]
    var playerName = ""
    var settings : Settings?
    var changeSubmarine = UIImageView()
    // MARK: - IBOUtlets
    @IBOutlet weak var leadingConstraintImageView: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!
    
    @IBOutlet weak var leftChangeImageView: UIImageView!
    
    @IBOutlet weak var backButton: UIButton!
    
    @IBOutlet weak var rightChangeImageView: UIImageView!
    
    @IBOutlet weak var descriptionSubmarine: UILabel!
    
    @IBOutlet weak var saveButton: UIButton!
    
    @IBOutlet weak var seaImageView: UIImageView!
    
    @IBOutlet weak var redAlertLabel: UILabel!
    
    @IBOutlet weak var playerLanel: UILabel!
    
    @IBOutlet weak var playerTextField: UITextField!
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let settings = UserDefaults.standard.value(Settings.self, forKey: "SetSet") else {return}
        self.settings = settings
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.playerTextField.text = settings?.name
        self.seaImageView.frame.origin = CGPoint(x: -40, y: -20)
        self.seaImageView.image = UIImage(named: "bottomThird")
        self.changeSubmarine.image = UIImage(named: Submarine.allCases.first!.rawValue)      //"twoStartSubmarine")
        super.viewWillAppear(animated)
        self.backButton.roundCorners(15)
        changeSubmarine.frame.size = containerView.frame.size
        changeSubmarine.frame.origin = CGPoint(x: 0, y: 0)
        containerView.addSubview(changeSubmarine)
        //        self.seaImageView.addSubview(self.changeSubmarine)
        moveThirdSea(seaView: seaImageView)
        self.redAlertLabel.textDropShadow(opacity: 6)
        self.redAlertLabel.dropShadow(colors: UIColor.white.cgColor, opacity: 1)
        self.playerLanel.layer.cornerRadius = 15
        let leftRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(leftSwipeSelectSubmarine(_:)))
        leftRecognizer.direction = .left
        containerView.addGestureRecognizer(leftRecognizer)
        let rightRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(rightSwipeRecognizer))
        rightRecognizer.direction = .right
        self.changeSubmarine.addGestureRecognizer(rightRecognizer)
        self.changeSubmarine.isUserInteractionEnabled = true
        self.view.isUserInteractionEnabled = true
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addTapRecognizer(_:))))
        let playerTitle = "Player:".localized
        self.playerLanel.text = playerTitle
        let backTitle = "Back".localized
        self.backButton.setTitle(backTitle, for: .normal)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        guard let text = playerTextField.text else {return}
        settings = Settings(name: text, speed: 1, submarineImageName: Submarine.allCases[currentIndex].rawValue)
        UserDefaults.standard.set(encodable: settings, forKey: "SetSet")
    }
    
    // MARK: - IBActions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        guard   let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {return}
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func addTapRecognizer(_ recognizer: UITapGestureRecognizer){
        self.view.endEditing(true)
    }
    
    @IBAction func rightSwipeRecognizer(_ recognizer: UISwipeGestureRecognizer){
        let newImageView = UIImageView()
        newImageView.frame.size = self.changeSubmarine.frame.size
        newImageView.frame.origin = CGPoint(x: 0 - newImageView.frame.width, y: self.changeSubmarine.frame.origin.y)
        newImageView.image = previousImage()
        self.containerView.addSubview(newImageView)
        self.containerView.clipsToBounds = true
        UIView.animate(withDuration: 0.5, delay: 0, options: .curveLinear) {
            self.changeSubmarine.frame.origin.x = self.changeSubmarine.frame.width
            newImageView.frame.origin.x = 0
        } completion: { _ in
            self.changeSubmarine.image = newImageView.image
            self.changeSubmarine.frame = newImageView.frame
            newImageView.removeFromSuperview()
        }
    }
    
    @IBAction  func leftSwipeSelectSubmarine(_ recognizer: UISwipeGestureRecognizer){
        let newImageView = UIImageView()
        newImageView.frame.size = self.changeSubmarine.frame.size
        newImageView.frame.origin = CGPoint(x: self.changeSubmarine.frame.width, y: self.changeSubmarine.frame.origin.y)
        newImageView.image = nextImage()
        self.containerView.addSubview(newImageView)
        self.containerView.clipsToBounds = true
        print(self.changeSubmarine.frame.origin)
        //      self.leadingConstraintImageView.constant -= self.changeSubmarine.frame.width
        
        UIView.animate(withDuration: 0.5) {
            self.changeSubmarine.frame.origin.x = 0 - self.changeSubmarine.frame.width
            newImageView.frame.origin.x -= newImageView.frame.width
            //          self.leadingConstraintImageView.constant -= self.changeSubmarine.frame.width
            //          self.view.layoutIfNeeded()
        } completion: { _ in
            //          self.changeSubmarine.removeFromSuperview()
            self.changeSubmarine.image = newImageView.image
            self.changeSubmarine.frame = newImageView.frame
            newImageView.removeFromSuperview()
        }
        
        
    }
    
    // MARK: - flow func
    
    func nextImage() -> UIImage {
        currentIndex += 1
        if currentIndex >= Submarine.allCases.count {
            currentIndex = 0
        }
        return UIImage(named: Submarine.allCases[currentIndex].rawValue)!
    }
    func previousImage() -> UIImage {
        currentIndex -= 1
        if currentIndex < 0 {
            currentIndex = Submarine.allCases.count - 1
        }
        return UIImage(named: Submarine.allCases[currentIndex].rawValue)!
    }
    
}
extension SetSettingsViewController : UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
}
