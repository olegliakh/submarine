//
//  RecordViewModel.swift
//  YellowSubmarine
//
//  Created by Oleg Liakh on 18.04.22.
//

import Foundation
import UIKit
import RxCocoa
import RxSwift

class RecordViewModel {
    
    private var recordsArray = [Record]()
    var data =  BehaviorRelay<[Record]>(value: [])
    
    init (){
//        data.accept(recordsArray)
    }
    
    func setRecords(array: [Record]) {
        recordsArray = array
        data.accept(recordsArray)
    }
}
