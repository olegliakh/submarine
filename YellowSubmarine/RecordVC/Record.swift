
import Foundation
import RxCocoa
import RxSwift
import UIKit

class Record: Codable {
  
    var playerName : String
    var result : Int
    var date : Date
 
    
    
    init(playerName: String, date: Date, result: Int) {
        self.playerName = playerName
        self.date = date //Date()
        self.result = result
    }
    
    private enum CodingKeys: String, CodingKey {
        case name
        case date
        case result
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(playerName, forKey: .name)
        try container.encode(date, forKey: .date )
        try container.encode(result, forKey: .result)
    }
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        self.playerName = try container.decode(String.self, forKey: .name)
        self.date = try  container.decode(Date.self, forKey: .date)
        self.result = try  container.decode(Int.self, forKey: .result)
    }
}
