//
//  RecordTableViewCell.swift
//  YellowSubmarine
//
//  Created by Oleg Liakh on 7.02.22.
//

import UIKit
import RxCocoa
import RxSwift
import UIKit

class RecordTableViewCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var resultLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    static let identifier =  "RecordTableViewCell"
    
   
    
    func configure (name: String, result: Int, date: Date) {
        self.nameLabel.text = name
        self.resultLabel.text = String(result)
        let formater = DateFormatter ()
        formater.dateFormat = "dd.MM.yy HH:mm"
        formater.locale = Locale(identifier: "en_US")
        self.dateLabel.text = formater.string(from: date)
    }
}
