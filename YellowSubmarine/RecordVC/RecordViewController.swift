//
//  RecordViewController.swift
//  YellowSubmarine
//
//  Created by Oleg Liakh on 22.01.22.
//
import Foundation
import UIKit
import RxCocoa
import RxSwift


// MARK: - class
class RecordViewController: UIViewController {
    // MARK: - lets/vars
    var recordsArray = [Record]()
    var viewModel = RecordViewModel()
    let disposeBag = DisposeBag()
    
    
    
    
    
    // MARK: - IBOUtlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var seaImageView: UIImageView!
    @IBOutlet weak var backButton: UIButton!
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.setRecords(array: loadRecords())        
        config()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.seaImageView.image = UIImage(named: "bottomThird")
        //        self.recordsArray = loadRecords()
        moveThirdSea(seaView: seaImageView)
        self.backButton.roundCorners(15)
        self.tableView.roundCorners(15)
        let backTitle = "Back".localized
        self.backButton.setTitle(backTitle, for: .normal)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }
    // MARK: - IBActions
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
        guard let controller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else {return}
        self.navigationController?.popViewController(animated: true)
    }
    // MARK: - flow func
    
    private func config () {
        viewModel
            .data
            .bind(to: tableView.rx.items(cellIdentifier: RecordTableViewCell.identifier, cellType: RecordTableViewCell.self)) { index, record , cell  in
                cell.configure(name: record.playerName, result: record.result  , date:record.date )
            }
            .disposed(by: disposeBag)
    }
}


func loadRecords() -> [Record] {
    guard let records = UserDefaults.standard.value([Record].self, forKey: "records")
    else {return []}
    return records
}
//
//extension RecordViewController : UITableViewDelegate, UITableViewDataSource {
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecordTableViewCell", for: indexPath) as? RecordTableViewCell else {return UITableViewCell() }
//        cell.configure(name: recordsArray[indexPath.row].playerName, result: recordsArray[indexPath.row].result, date: recordsArray[indexPath.row].date)
//        return cell
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        print("number of rows", recordsArray.count)
//        return self.recordsArray.count
//    }
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//
//
//}
