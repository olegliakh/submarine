//
//  Settings.swift
//  YellowSubmarine
//
//  Created by Oleg Liakh on 13.01.22.
//

import Foundation
import CoreVideo
import UIKit


class Settings: Codable {
    var name: String
    var speed: Float?
    var submarineImageName: String
    
    init(name: String, speed: Float?, submarineImageName: String) {
        self.name = name
        self.speed = speed
        self.submarineImageName = submarineImageName
    } 
    
    private enum CodingKeys: String, CodingKey {
        case name
        case speed
        case submarineImageName
    }
    
    func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(speed, forKey: .speed)
        try container.encode(submarineImageName, forKey: .submarineImageName)
    }

   
   required init(from decoder: Decoder) throws {
       let container = try decoder.container(keyedBy: CodingKeys.self)
       self.name = try container.decode(String.self, forKey: .name)
       self.speed = try container.decodeIfPresent(Float.self, forKey: .speed)
       self.submarineImageName = try container.decode(String.self, forKey: .submarineImageName )
   }

}


enum Submarine : String, CaseIterable {
    case one = "submarine"
    case two = "twoStartSubmarine"
    
    func getImage()->UIImage? {
        switch self {
        case .one:
            return UIImage(named: "submarine")
        case .two:
            return UIImage(named: "twoStartSubmarine")
        }
    }
}
