import RxCocoa
import RxSwift
import Foundation

class RxTableModel {
    var recordsArray : [Record] = []
    var data =  BehaviorRelay<[Record]>(value: [])
    
    init (){
        data.accept(recordsArray)
    }
    
    func setRecords(array: [Record]) {
        recordsArray = array
    }
}
