//
//  UIview_extension.swift
//  YellowSubmarine
//
//  Created by Oleg Liakh on 23.12.21.
//

import Foundation
import UIKit

extension UIView {
    
    
   
    
    func roundCorners(_ radius: CGFloat = 15){
        self.layer.cornerRadius = radius
    }
    func dropShadow(colors: CGColor? , opacity: Float) {
        self.layer.masksToBounds = false
        self.layer.shadowColor =  UIColor.white.cgColor
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 2, height: 1)
        self.layer.shadowRadius = 16
        self.layer.shouldRasterize = true //пикселизация тени
        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath // для рисования внутри вью кривые бизье
    }
    func addGradient() {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.white.cgColor, UIColor.red.cgColor, UIColor.white.cgColor]
        gradient.opacity = 0.9
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        gradient.cornerRadius = bounds.width / 4.5
        self.clipsToBounds = true
        gradient.frame = self.bounds
        
        self.layer.insertSublayer(gradient, at: 0)
    }
    func funGradient(){
        let gradient = CAGradientLayer()
        gradient.colors = [ UIColor.systemBlue.cgColor,
                            UIColor.systemPink.cgColor,
                            UIColor.systemBlue.cgColor,
                            UIColor.systemPink.cgColor,
                            UIColor.systemBlue.cgColor,
                            UIColor.systemPink.cgColor,
                            UIColor.systemBlue.cgColor,
                            UIColor.systemPink.cgColor]
        gradient.locations   = [ 0,0.1,0.2,0.3,0.4,0.5,0.6,1 ]
            
    }
    func addGradientTwo(withRadius: CGFloat, opacity: Float ) {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.white.cgColor, UIColor.systemGreen.cgColor, UIColor.white.cgColor]
        gradient.opacity = 3
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        gradient.cornerRadius = 15 //bounds.width / withRadius
        self.clipsToBounds = true
        gradient.frame = self.bounds
        self.layer.insertSublayer(gradient, at: 0) }
        
        func addGradientFour(withRadius: CGFloat, opacity: Float ) {
            let gradient = CAGradientLayer()
            gradient.colors = [UIColor.white.cgColor, UIColor.black.cgColor, UIColor.white.cgColor]
            gradient.opacity = 3
            gradient.startPoint = CGPoint(x: 0, y: 0)
            gradient.endPoint = CGPoint(x: 1, y: 1)
            gradient.cornerRadius = 15 //bounds.width / withRadius
            self.clipsToBounds = true
            gradient.frame = self.bounds
            self.layer.insertSublayer(gradient, at: 0)
        
    }
    func addGradientThird(withRadius: CGFloat, opacity: Float ) {
        let gradient = CAGradientLayer()
        gradient.colors = [UIColor.white.cgColor, UIColor.red.cgColor, UIColor.white.cgColor]
        gradient.opacity = 0.7
        gradient.startPoint = CGPoint(x: 0, y: 0)
        gradient.endPoint = CGPoint(x: 1, y: 1)
        gradient.cornerRadius = 15 //bounds.width / 0.5
        self.clipsToBounds = true
        gradient.frame = self.bounds
        self.layer.insertSublayer(gradient, at: 0)
    }
}

extension UILabel {
    func textDropShadow(opacity: Float) {
//        self.layer.masksToBounds = false
//        self.layer.shadowRadius = 10.0
//        self.layer.shadowOpacity = 0.5
//        self.layer.shadowOffset = CGSize(width: 0 , height: 0)
        
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.blue.cgColor
        self.layer.shadowOpacity = 2.0 //1.0
        self.layer.shadowOffset = CGSize(width: 3, height: 2)
        self.layer.shadowRadius = 20//3.0
        
        self.layer.shouldRasterize = true
        
     //   self.layer.shadowPath =
        
//        self.layer.shadowPath = UIBezierPath(cgPath: CGPath( self.text))
        
    }

    static func createCustomLabel() -> UILabel {
        let label = UILabel()
        label.textDropShadow(opacity: 10)
        return label
    }
}
extension UILabel {
   func UILableTextShadow(color: UIColor){
//      self.textColor = color
       self.layer.shadowColor = color.cgColor
      self.layer.masksToBounds = false
      self.layer.shadowOffset = CGSize(width: 0, height: 0)
       self.layer.shouldRasterize = true
      self.layer.rasterizationScale = UIScreen.main.scale
      self.layer.shadowRadius = 10.0
       self.layer.shadowOpacity = 0.5
       self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
   }
}
extension UIButton {
    func setTitleWithAttributes(name: String, font: String, size: CGFloat) {
        
        let attributes : [NSAttributedString.Key : Any] = [
            .font: UIFont(name: font, size: size)
        ]
        let attributedString = NSAttributedString(string: name, attributes: attributes)
        self.setAttributedTitle(attributedString, for: .normal)
        
    }
}
extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
