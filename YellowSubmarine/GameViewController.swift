//
//  GameViewController.swift
//  YellowSubmarine
//
//  Created by Oleg Liakh on 12.12.21.
//

import UIKit
import CoreMotion
import AVFoundation


// MARK: - class
class GameViewController: UIViewController {
    
    // MARK: - lets/vars
    var manager = CMMotionManager()
    var healthAmount =  UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var curent = 0
    let shipImages : [String] = ["so0","so1","so2","so3","so4"]
    let fishImages : [String] = ["fish", "fishTwo", "fishThree", "akvaMan"]
    let submarineImage : String = ""
    let airImage : String = ""
    var tomagavkPresentation : UIImageView?
    var timeControl : Timer?
    var killFishImage : CGFloat?
    var shipImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var submarineImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
    var playerName : String?
    var player: AVAudioPlayer?
    var playerTwo: AVAudioPlayer?
    var playerThird: AVAudioPlayer?
    var playerFour: AVAudioPlayer?
    var settings = Settings(name: "", speed: 1, submarineImageName: "red")
    // MARK: - IBOUtlets
    
    

    @IBOutlet weak var playerLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var skyImageView: UIImageView!
    @IBOutlet weak var fishImageView: UIImageView!
    @IBOutlet weak var tomagavkImageView: UIImageView!
    @IBOutlet weak var fireButton: UIButton!
    @IBOutlet weak var healtsSubmarineLabel: UILabel!
    @IBOutlet weak var healtsView: UIView!
    @IBOutlet weak var airImageView: UIImageView!
    @IBOutlet weak var upButton: UIButton!
    @IBOutlet weak var bottomImageView: UIImageView!
    @IBOutlet weak var downButton: UIButton!
    @IBOutlet weak var seaImageView: UIImageView!
    
    
    // MARK: - lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        if manager.isAccelerometerAvailable {
            manager.accelerometerUpdateInterval = 0.1
            manager.startAccelerometerUpdates(to: .main) {[weak self] data, error  in
                if let acceleration = data?.acceleration {
                    debugPrint("x: \(acceleration.x)")
                    debugPrint("y: \(acceleration.y)")
                    debugPrint("z: \(acceleration.z)")
                    debugPrint("")
                }
                    
            }
        }
        if manager.isGyroAvailable{
            manager.gyroUpdateInterval = 0.1
            manager.startGyroUpdates(to: .main) { [weak self] data, error in
                if let gyro = data?.rotationRate {
                    debugPrint("x: \(gyro.x)")
                    debugPrint("y: \(gyro.y)")
                    debugPrint("z: \(gyro.z)")
                    debugPrint("")
                    
                }
            }
        }
        
        
        
        
        
        
        
        
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        playBackground()
        self.submarineImageView.frame.size = CGSize(width: 150, height:50)
        self.healtsSubmarineLabel.textDropShadow(opacity: 4.0)
        self.fireButton.roundCorners(15)
        self.fireButton.addGradientFour(withRadius: 15, opacity: 3)
        self.fireButton.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        self.fireButton.titleLabel?.textDropShadow(opacity: 0.5)
        self.submarineImageView.image = UIImage(named: settings.submarineImageName)
        self.healtsView.layer.cornerRadius = 15
        self.skyImageView.addSubview(self.healtsView)
        self.healtsView.dropShadow(colors: UIColor.white.cgColor, opacity: 3)
        self.skyImageView.image = UIImage(named: "sky")
        self.skyImageView.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        self.bottomImageView.image = UIImage(named: "bottom")
        self.seaImageView.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        self.seaImageView.image = UIImage(named: "sea")
        self.seaImageView.addSubview(self.shipImageView)
        self.seaImageView.bringSubviewToFront(self.shipImageView)
        self.seaImageView.addSubview(self.fishImageView)
        self.seaImageView.bringSubviewToFront(self.fishImageView)
        self.timeToStart()
        self.healtsView.backgroundColor = .red
        self.healthAmount.frame = self.healtsView.frame
        healthAmount.backgroundColor = .green
        self.healthAmount.roundCorners(15)
        self.view.addSubview(healthAmount)
        self.downButton.addGradient()
        self.upButton.addGradientTwo(withRadius: 4.5, opacity: 3)
        self.downButton.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        self.upButton.dropShadow(colors: UIColor.white.cgColor, opacity: 0.5)
        self.seaImageView.clipsToBounds = false
        self.downButton.layer.cornerRadius = 15
        self.upButton.layer.cornerRadius = 15
        self.submarineImageView.frame.origin = CGPoint(x: 180, y: 50)
        self.seaImageView.addSubview(self.submarineImageView)
        self.moveSeaTwo()
      self.playerLabel.roundCorners(15)
        self.scoreLabel.roundCorners(15)
        self.upButton.setTitleWithAttributes(name: "Up".localized, font: fontName, size: 20)
        self.downButton.setTitleWithAttributes(name: "Down".localized, font: fontName, size: 20)
        print(tomagavkImageView.frame)
        print(submarineImageView.frame)
        self.playerLabel.clipsToBounds = true
        self.scoreLabel.layer.cornerRadius = 15
        self.scoreLabel.clipsToBounds = true
        self.scoreLabel.frame.origin = CGPoint(x: self.skyImageView.frame.maxX - 80, y: self.skyImageView.frame.minY - 90)
        self.scoreLabel.frame.size = CGSize(width: 70, height: 25)
        self.seaImageView.addSubview(scoreLabel)
        self.playerLabel.text = "PLAYER: " + settings.name
        let downTitle = "Down".localized
        self.downButton.setTitle(downTitle, for: .normal)
        let fireTitle = "Fire".localized
        self.fireButton.setTitle(fireTitle, for: .normal)
        let upTitle = "Up".localized
        self.upButton.setTitle(upTitle, for: .normal)
        let playerTitle = "Player:".localized
        self.playerLabel.text = playerTitle
        let healthTitle = "Health Submarine".localized
        self.healtsSubmarineLabel.text = healthTitle
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.stopTimerTest()
    }
    
    // MARK: - IBActions
    
    @IBAction func downButtonPressed(_ sender: UIButton) {
        print(self.submarineImageView.frame)
        print(self.seaImageView.frame)
        if self.submarineImageView.frame.origin.y + self.submarineImageView.frame.height + 20 < self.seaImageView.frame.height {
            UIImageView.animate(withDuration: 0.3) {
                self.submarineImageView.frame.origin.y += 20
                self.submarineImageView.frame.origin.x += 2
                
            } completion: { _ in
                print(self.submarineImageView.frame)
                print(self.seaImageView.frame)
                
            }
        }
    }
    @IBAction func fireButtonPressed(_ sender: Any) {
        let tomagavkImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        tomagavkImageView.frame.origin = CGPoint(x: self.submarineImageView.frame.origin.x, y: self.submarineImageView.frame.maxY )
        tomagavkImageView.image = UIImage(named: "tomagavk")
        tomagavkImageView.frame.size = CGSize(width: 50, height: 25)
        tomagavkImageView.contentMode = .scaleAspectFit
        self.seaImageView.addSubview(tomagavkImageView)
        self.tomagavkPresentation = tomagavkImageView
        playGoGoTorpeda()
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
            tomagavkImageView.frame.origin.x =  self.submarineImageView.frame.origin.x
        } completion: { _ in
            UIView.animate(withDuration: 5, delay: 0, options: .curveLinear) {
                tomagavkImageView.frame.origin.x =    self.seaImageView.frame.maxX
                
            }
        }
    }
    
    
    @IBAction func upButtonPressed(_ sender: UIButton) {
        print(self.seaImageView.frame.height)
        print(self.seaImageView.frame.minY)
        print(self.seaImageView.frame.maxY)
    
        if self.submarineImageView.frame.origin.y + self.submarineImageView.frame.height - 20 > 0 {
            UIImageView.animate(withDuration: 0.3) {
                self.submarineImageView.frame.origin.y -= 20
                self.submarineImageView.frame.origin.x += 5
            } }
    }
    // MARK: - flow func
    
    private func playGoGoTorpeda() {
        guard let url = Bundle.main.url(forResource: "плыветТорпеда", withExtension: "mp3") else  { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            try AVAudioSession.sharedInstance().setActive(true, options: .init())
            
            player = try AVAudioPlayer(contentsOf: url)
            player?.play()
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    private func playOver() {
        guard let url = Bundle.main.url(forResource: "киллБоат", withExtension: "mp3") else  { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            try AVAudioSession.sharedInstance().setActive(true, options: .init())
            
            playerThird = try AVAudioPlayer(contentsOf: url)
            playerThird?.play()
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    private func playBoom() {
        guard let url = Bundle.main.url(forResource: "взрывТорпеды", withExtension: "mp3") else  { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            try AVAudioSession.sharedInstance().setActive(true, options: .init())
            
            player = try AVAudioPlayer(contentsOf: url)
            player?.play()
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    private func playBackground() {
        guard let url = Bundle.main.url(forResource: "основная", withExtension: "mp3") else  { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            try AVAudioSession.sharedInstance().setActive(true, options: .init())
            
            playerTwo = try AVAudioPlayer(contentsOf: url)
            playerTwo?.play()
        } catch let error {
            print(error.localizedDescription)
        }
        
    }
    
    private func playALert() {
        guard let url = Bundle.main.url(forResource: "столкновениеОбьект", withExtension: "mp3") else  { return }
        
        do {
            try AVAudioSession.sharedInstance().setCategory(.playback)
            try AVAudioSession.sharedInstance().setActive(true, options: .init())
            
            playerFour = try AVAudioPlayer(contentsOf: url)
            playerFour?.play()
        } catch let error {
            print(error.localizedDescription)
        }
        var runCount = 0
        var timer = Timer.scheduledTimer(withTimeInterval: 0.9, repeats: true) { timer in
            print("Timer fired!")
              runCount += 1

              if runCount == 3 {
                  self.playerFour?.stop()
                  timer.invalidate()
              }
         
        }
    }
    
    
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        if event?.subtype == UIEvent.EventSubtype.motionShake {
            print("up")
            self.upSubmarine()
            
        }
    }
    
    func moveShip(){
        let shipImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        shipImageView.frame.size = CGSize(width: 120 , height: 60)
        guard let randomShipImage = shipImages.randomElement() else {return}
        shipImageView.image = UIImage(named: randomShipImage)
        shipImageView.contentMode = .scaleAspectFit
        shipImageView.frame.origin = CGPoint(x: self.seaImageView.frame.width, y: 0 - 0.6 * shipImageView.frame.height)
        self.seaImageView.addSubview(self.shipImageView)
        self.seaImageView.bringSubviewToFront(shipImageView)
        let timerTwo = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { _ in
            UIView.animate(withDuration: 10, delay: 0, options: .curveLinear) {
                shipImageView.frame.origin.x = shipImageView.frame.origin.x - shipImageView.frame.width
                guard let shipPresentation = shipImageView.layer.presentation(),
                      let submarinePresentation = self.submarineImageView.layer.presentation() else {return}
                if shipPresentation.frame.intersects(submarinePresentation.frame)
                {
                    print("ALARMALARMALARMALARM")
                    self.killMyBoat()
                }
            } completion: { _ in
                
            }
            if shipImageView.frame.origin.x + shipImageView.frame.width < 0 {
            }
        }
        self.seaImageView.addSubview(shipImageView)
    }
    
    func moveFish() {
        
        let fishImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
        fishImageView.frame.size = CGSize(width: 70, height: 30)
        fishImageView.frame.origin.x = self.seaImageView.frame.width //- fishImageView.frame.width
        fishImageView.frame.origin.y = CGFloat.random(in: 0.0...(self.seaImageView.frame.height - fishImageView.frame.height))
        guard let randomFishObject = fishImages.randomElement() else {return}
        fishImageView.image = UIImage(named: randomFishObject )
        self.seaImageView.addSubview(fishImageView)
        print("NEW fish moves")
        fishImageView.contentMode = .scaleAspectFit
        let timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) { timer in
            UIView.animate(withDuration: 2, delay: 0, options: .curveLinear) {
                fishImageView.frame.origin.x = fishImageView.frame.origin.x  - fishImageView.frame.width
            }
            guard let fishPresentation = fishImageView.layer.presentation(),
                  let submarinePresentation = self.submarineImageView.layer.presentation() else {return}
            if fishPresentation.frame.intersects(submarinePresentation.frame)
            {
                print("intersects submarine")
                self.killMyFish(fish: fishImageView)
                self.healthSubmarine()
                self.playALert()
               
                timer.invalidate()
            }
            
            
            guard let tomohui = self.tomagavkPresentation?.layer.presentation() else {return}
            
            if fishPresentation.frame.intersects(tomohui.frame) {
            
               
            
                
                self.playBoom()
                print("OKO")
                self.killMyFish(fish: fishImageView)
                self.curent += 30
                self.scoreLabel.text = String(self.curent)
                
                self.tomagavkPresentation?.removeFromSuperview()
            }
            
        }
    }
    
    func upSubmarine(){
    if self.submarineImageView.frame.origin.y + self.submarineImageView.frame.height - 20 > 0 {
        UIImageView.animate(withDuration: 0.3) {
            self.submarineImageView.frame.origin.y -= 75
            self.submarineImageView.frame.origin.x += 5
        } }}
    
    func timeToStart() {
        guard let randomEventObject = (1...4).randomElement() else {return}
        var  nextEventTime = randomEventObject
        var currentTime = 0
        var numerOfObjects = 0
        guard timeControl == nil else { return }
        timeControl = Timer.scheduledTimer(withTimeInterval: 1, repeats: true) { Timer in
            currentTime += 1
            print(currentTime)
            if currentTime == nextEventTime {
                numerOfObjects += 1
                print(" SOVPALO! nextEventTime \(nextEventTime) ")
                switch numerOfObjects % 2 {
                case 0 :
                    print(currentTime, "FISH")
                    self.moveFish()
                case 1:
                    print(currentTime, "SHIP")
                    self.moveShip()
                default:
                    break
                }
                guard let randomInterval = (3...5).randomElement() else {return}
                nextEventTime = randomInterval + currentTime
                
            }
        } }
    
    func healthSubmarine () {
        UIView.animate(withDuration: 0.5) {
            self.healthAmount.frame.size.width -= 0.05 * self.healtsView.frame.width
        }  completion: { _ in
            print("HEALTH", self.healthAmount.frame)
            
            if self.healthAmount.frame.size.width <= 0 {
                self.killMyBoat()
            }
        }
        

        }
    
    
    
    func stopTimerTest () {
        timeControl?.invalidate()
        timeControl = nil
        player?.stop()
        playerTwo?.stop()
        playerFour?.stop()
        playerThird?.stop()
    }
    
    func killMyFish (fish: UIImageView) {
//        let boomImageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
//        boomImageView.frame.origin = CGPoint(x: (fish.frame.origin.x), y: (fish.frame.maxY) )
//        boomImageView.image = UIImage(named: "взрыв")
//        boomImageView.frame.size = CGSize(width: 10, height: 10)
//        boomImageView.contentMode = .scaleAspectFit
//        self.seaImageView.addSubview(boomImageView)
        UIView.animate(withDuration: 6, delay: 0, options: .curveLinear) {
            fish.frame.origin.y = self.view.frame.maxY
            
        } completion: { _ in
            fish.removeFromSuperview()
          
        }
    
    }
    
    func killMyBoat(){
        self.upButton.isEnabled = false
        self.downButton.isEnabled = false
        UIView.animate(withDuration: 5) {
            self.submarineImageView.frame.origin.y = self.view.frame.maxY
        } completion: { _ in
            self.submarineImageView.removeFromSuperview()
            self.stopTimerTest()
            self.addNewRecord()
            self.playerTwo?.stop()
            self.navigationController?.popViewController(animated: true)
        }
        playOver()
    }
    
    func addNewRecord() {
        var recordsArray = loadRecords()
        
        guard let scoreText = scoreLabel.text else {return}
        let score = Int(scoreText) ?? 0
        let newRecord = Record(playerName: self.settings.name, date: Date(), result: score)
        recordsArray.append(newRecord)
        
        UserDefaults.standard.set(encodable: recordsArray, forKey: "records")
    }
    func  moveSeaTwo () {
        UIView.animate(withDuration: 4, delay: 0, options: .curveLinear) {
            self.seaImageView.frame.origin.x -= 15
        } completion: { _ in
            UIView.animate(withDuration: 9, delay: 0, options: .curveLinear) {
                self.seaImageView.frame.origin.x += 15
            } completion: { _ in
                self.moveSeaTwo()
            }
        }
    }
}
